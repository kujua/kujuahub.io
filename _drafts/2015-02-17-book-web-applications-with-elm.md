---
layout: post
title: Book - Web applications with Elm
photo-front-url: /res/wrapper_elm_title.png
photo-front-class: grid-image-wrapper-square
sortorder: 9025
date: 2015-06-04
ignoredate: 1
ignoreall: 1
---

**Elm is a functional language based on the concept of Functional Reactive Programming.  I am using Elm and its libraries to develop an editor for a content management system. And I am writing a book about it.**

Coming
