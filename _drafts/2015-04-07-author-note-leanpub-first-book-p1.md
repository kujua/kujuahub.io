---
layout: post
title: Author's Notes
date: 2015-05-04
sortorder: 4085
---

**A few days ago I published the first chapters of my first book on Leanpub. This post describes my experience of writing a book.**

Lean publishing is a term that is used by [Leanpub](http://leanpub.com) to describe an agile process of publishing a book.
