---
layout: post
title: MongoDB's hidden feature - GridFS
sortorder: 4075
date: 2015-06-22
ignoredate: 0
---

**One of the features of MongoDB is GridFS, a specialized collection for binary files. There is not much discussion about this feature and it seems that GridFS is not used widely.**

Coming Soon
