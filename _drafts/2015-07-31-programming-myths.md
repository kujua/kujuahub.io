---
layout: post
title: Programming Myths
sortorder: 4082
date: 2015-07-31
ignoredate: 0
disqus_comments: 1
disqus_identifier: 2015-07-31-programming-myths
---

**Coming to a new project and analyzing the existing solution and code base, I try to question some design decisions and often hear: "It must be done like this."**

I am always astonished by the rigidity of those believes. Of course, developers like to sail in safe waters. We all have done it, copying a solution that worked in a project before and just applying the concept without thinking too much.


data first
dependency injection
