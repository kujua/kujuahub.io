---
layout: post
title: Book - Personal History of Programming Languages
sortorder: 20
date: 2015-06-14
ignoredate: 1
---

**After more than 30 years programming I am looking back at all the programming languages I have (or had to) use.**

It is quite a number of programming languages we developers, software engineers, programmers or hackers - whatever your preferred title - use to deliver quality to our customers or just play around with.

Some time ago I decided to write a book about my own personal history of programming languages. It spans from 6502 assembler to Elixir, from C to F#. Sometimes the language is only part of a bigger system and so I am also writing about some of the frameworks that are bound to those languages.

It will take a while to bring together all this under time constraints of ongoing projects and it is quite possible that another language or two might join the catalog during writing.

First chapters will be available in the second half of 2015.
